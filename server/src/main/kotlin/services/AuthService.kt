package services

import io.grpc.Server
import io.grpc.ServerBuilder
import io.grpc.kotlin.AbstractCoroutineServerImpl
import io.grpc.protobuf.services.ProtoReflectionService
import src.main.proto.auth.*


class AuthServer (
    private val port: Int,
    private val services: List<AbstractCoroutineServerImpl>
){
    private lateinit var server: Server

    private val serverBuilder = ServerBuilder
        .forPort(port)
        .addService(ProtoReflectionService.newInstance())

//        .addService(AuthServe())
//        .addService(ProtoReflectionService.newInstance())
//        .build()

    private fun addServices(_services: List<AbstractCoroutineServerImpl>): Server =
        serverBuilder.let { builder ->
            _services.map {
                builder.addService(it)
            }
            builder.build()
        }

    fun start() {
        server = addServices(services)

        server.start()

        println("Server started, listening on $port")
        Runtime.getRuntime().addShutdownHook(
            Thread {
                println("*** shutting down gRPC server since JVM is shutting down")
                this@AuthServer.stop()
                println("*** server shut down")
            }
        )
    }

    private fun stop() {
        server.shutdown()
    }

    fun blockUntilShutdown() {
        server.awaitTermination()
    }

}