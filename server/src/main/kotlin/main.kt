package server

import com.google.protobuf.Descriptors
import io.grpc.kotlin.AbstractCoroutineServerImpl
import services.AuthServer
import src.main.proto.auth.AuthGrpcKt
import src.main.proto.auth.UserData
import src.main.proto.auth.Token


fun main() {
    val authClient = AuthClient(
        realmName = "sea-point-teste",
        KeycloakCredentials(
            username = "testador_de_api",
            password = "123",
            clientId = "api",
            clientSecret = "10c23088-c0ae-4853-ab85-a26fc6c4e36d",
        )
    )

    // val TOKEN = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJFSkZlVUJmaXB1SWlOYjg3RkQwOHdfa1FPWHFGNmRGMDZZUDdiNERZS1pFIn0.eyJleHAiOjE2Mjg5MDk4NTYsImlhdCI6MTYyODg3Mzg1NiwianRpIjoiZDgyMGZmMTUtY2U2NC00NzliLTg2YzUtNGQ0MmNlYTcwNjMyIiwiaXNzIjoiaHR0cHM6Ly9hdXRoLnNlYXNvbHV0aW9ucy5jb20uYnIvYXV0aC9yZWFsbXMvc2VhLXBvaW50LXRlc3RlIiwiYXVkIjpbInJlYWxtLW1hbmFnZW1lbnQiLCJhcGkiLCJhY2NvdW50Il0sInN1YiI6Ijc5ZTE1NzUxLWQwZjktNGU5OS1hNjc0LTFhNWI5NmQ1Y2M1ZiIsInR5cCI6IkJlYXJlciIsImF6cCI6IndlYi1hcHAiLCJzZXNzaW9uX3N0YXRlIjoiNDc3YzNmZDUtNjMxNC00ZDBkLTk3MTgtMDI4ZTU4ODZiODZhIiwiYWNyIjoiMSIsImFsbG93ZWQtb3JpZ2lucyI6WyIqIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsid2ViLWFwcCI6eyJyb2xlcyI6WyJ1bWFfcHJvdGVjdGlvbiJdfSwicmVhbG0tbWFuYWdlbWVudCI6eyJyb2xlcyI6WyJ2aWV3LWlkZW50aXR5LXByb3ZpZGVycyIsInZpZXctcmVhbG0iLCJtYW5hZ2UtaWRlbnRpdHktcHJvdmlkZXJzIiwiaW1wZXJzb25hdGlvbiIsInJlYWxtLWFkbWluIiwiY3JlYXRlLWNsaWVudCIsIm1hbmFnZS11c2VycyIsInF1ZXJ5LXJlYWxtcyIsInZpZXctYXV0aG9yaXphdGlvbiIsInF1ZXJ5LWNsaWVudHMiLCJxdWVyeS11c2VycyIsIm1hbmFnZS1ldmVudHMiLCJtYW5hZ2UtcmVhbG0iLCJ2aWV3LWV2ZW50cyIsInZpZXctdXNlcnMiLCJ2aWV3LWNsaWVudHMiLCJtYW5hZ2UtYXV0aG9yaXphdGlvbiIsIm1hbmFnZS1jbGllbnRzIiwicXVlcnktZ3JvdXBzIl19LCJhcGkiOnsicm9sZXMiOlsidW1hX3Byb3RlY3Rpb24iXX0sImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJ2aWV3LWFwcGxpY2F0aW9ucyIsInZpZXctY29uc2VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwibWFuYWdlLWNvbnNlbnQiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6ImVtYWlsIGdyb3VwIHByb2ZpbGUiLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwibmFtZSI6Ill1cmkgTGVtb3MiLCJncm91cHMiOlsiL0RldlRlc3QiXSwicHJlZmVycmVkX3VzZXJuYW1lIjoidGVzdGFkb3JfZGVfYXBpIiwiZ2l2ZW5fbmFtZSI6Ill1cmkiLCJmYW1pbHlfbmFtZSI6IkxlbW9zIiwiZW1haWwiOiJ5dXJpLnlsckBvdXRsb29rLmNvbSJ9.gWszoZ7QXcf-qSF6z5bUSMuCqKfgBhwJMNf4_Of0pt7diZmXFLvWpFoCSMmZDG5_qVBcnlt8Y5Ey3x3MpH6F5rMtUyAHNM2DC4NIJvYKQ521TjrBPfu0aH_1HGHKb9NnSNrQMNgb3vZFi52xH6AEEONKYl-8mpSrH9rI6-PMkGHhaheKr-Es940VhvGZwh_uorgZod3bzBUmaiG3QnB7LIpRMycUSeGrULpmrXbov3APKgp1lKbJ88w0asHOLm8p_eSFXMw78yjiBBbNAAItJGs5D-owRkFSzZTiWDIKbc5RK1EDSIhE7c35R14p9P3-sLsFQhWoK7it_-lCJATVyw"

//    val validate = authClient.validateToken(
//        TOKEN, UserData.newBuilder()
//    ).let { (userData, err) ->
//        userData
//    }

    val services = listOf<AbstractCoroutineServerImpl>(
        object : AuthGrpcKt.AuthCoroutineImplBase() {
            override suspend fun decryptionToken(request: Token): UserData {
                return authClient.validateToken(
                    request.data, UserData.newBuilder()
                )
            }
        },
    )

    /** Initialize gRPC server with service list */
    val server = AuthServer(8800,services)

    server.start()
    server.blockUntilShutdown()

//    val clientList = "Clients: " + authClient.takeAllClients().map { it ->
//        "${it.clientId} "
//    }.reduce { acc, it -> acc + it }
}