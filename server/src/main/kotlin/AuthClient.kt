package server

import com.auth0.jwt.JWT
import com.auth0.jwt.interfaces.Claim
import org.keycloak.OAuth2Constants
import org.keycloak.admin.client.KeycloakBuilder
import org.keycloak.admin.client.resource.RealmResource
import org.keycloak.representations.idm.ClientRepresentation

import java.util.logging.Logger

import io.github.cdimascio.dotenv.dotenv
import org.apache.http.protocol.HTTP
import src.main.proto.auth.Error
import src.main.proto.auth.UserData
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse


val logger: Logger = Logger.getLogger("main")

data class KeycloakCredentials(
    val username: String,
    val password: String,
    val clientId: String,
    val clientSecret: String,
)

private val jwtDecrypt = object {
//    val publicKey: String = dotenv()["PUBLIC_KEY"]
    val secretKey: String = dotenv()["SECRET_KEY"]
}

class AuthClient (
    private val realmName: String,
    private val credentialData: KeycloakCredentials
) {
    private lateinit var token: String

    private val realmResource: RealmResource = KeycloakBuilder.builder().let { builder ->
        val client = builder.serverUrl("https://auth.seasolutions.com.br/auth")
            .realm(realmName)
            .grantType(OAuth2Constants.PASSWORD)
            .username(credentialData.username)
            .password(credentialData.password)
            .clientId(credentialData.clientId)
            .clientSecret(credentialData.clientSecret)
            .build()

        logger.info("Keycloak client has been created")

        client.realm(realmName)
    }

    fun takeAllClients(): List<ClientRepresentation> =
        realmResource.clients().findAll()


    private fun decryptToken(_token: String): Map<String, Claim> = JWT.decode(_token).claims

    private fun validateSession(): Boolean {
        val response = HttpClient.newBuilder().build().let { client ->
            val realm = realmName
            val baseUri = "https://auth.seasolutions.com.br/auth"
            val uri = "$baseUri/realms/$realm/protocol/openid-connect/token/introspect"

            client.send(
                HttpRequest.newBuilder()
                    .uri(URI.create(uri))
                    .setHeader("Authorization", token)
                    .setHeader("Content-Type", "application/x-www-form-urlencoded")
                    .POST(
                        HttpRequest.BodyPublishers.ofString("""
                        client_id=api&client_secret=${jwtDecrypt.secretKey}&token=$token
                        """.trimIndent())
                    )
                    .build(),

                HttpResponse.BodyHandlers.ofString()
            )
        }

        return response.body()!!
            .toString()
            .contains("true")
    }

    fun validateToken(_token: String, userDataBuilder: UserData.Builder): UserData {
        this.token = _token

        if (!validateSession()) {
            return userDataBuilder.build()
        }

        decryptToken(token).forEach { (k, v) ->
            userDataBuilder.let {
                val fieldDescriptor =  it.descriptorForType.findFieldByName(k)

                val item = arrayListOf(
                    v.asList(String::class.java),
                    v.asString(),
                    v.asBoolean(),
                ).filterNotNull().firstOrNull()

                if (fieldDescriptor != null) {
                    it.setField(
                        fieldDescriptor,
                        item
                    )
                }
            }
        }

        return userDataBuilder.build()
    }

    //        .let { realm ->
//            val clients = realm.clients().findAll()
//
//            logger.info("Clients: ${clients.map {
//                it.clientId.toString() + "\n"
//            }}")
//
//            realm
//        }

}