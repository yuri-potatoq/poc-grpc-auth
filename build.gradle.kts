import com.google.protobuf.gradle.*

plugins {
    id("com.google.protobuf") version "0.8.17" apply false
    kotlin("jvm") version "1.4.31" apply false

    idea
    java
    application
}

// todo: move to subprojects, but how?
ext["grpcVersion"] = "1.39.0"
ext["grpcKotlinVersion"] = "1.1.0" // CURRENT_GRPC_KOTLIN_VERSION
ext["protobufVersion"] = "3.15.8"

group = "me.potatoq"
version = "1.0-SNAPSHOT"

allprojects {
    repositories {
        mavenCentral()
    }
}

//dependencies {
//    implementation (project(":server"))
//    implementation (project(":protos"))
//}

//sourceSets.main {
//    java.srcDirs("src/main/kotlin")
//}

//tasks.withType<Jar> {
//    manifest {
//        attributes["Main-Class"] = "server.MainKt"
//    }
//
//    configurations["compileClasspath"].forEach { file: File ->
//        from(zipTree(file.absoluteFile))
//    }

//    subprojects.forEach { subproject ->
//        subproject.configurations["compile"].forEach { file: File ->
//            from(zipTree(file.absoluteFile))
//        }
//    }
//}


tasks.test {
    useJUnit()
}