plugins {
    kotlin("jvm")

    application
    java
    idea
}

application {
    mainClass.set("server.MainKt")
}

dependencies {
    implementation(project(":protos"))

    implementation ("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.5.1")


    implementation ("com.google.protobuf:protobuf-java:${rootProject.ext["protobufVersion"]}")
    implementation ("io.grpc:grpc-stub:1.39.0")
    implementation ("io.grpc:grpc-kotlin-stub:1.1.0")
    implementation ("io.grpc:grpc-services:1.39.0")
    implementation ("io.grpc:grpc-netty-shaded:1.39.0")
    implementation ("io.grpc:grpc-netty:1.39.0")
    implementation ("io.grpc:grpc-kotlin-stub:1.1.0")

    implementation ("org.keycloak:keycloak-admin-client:15.0.0")

    implementation("io.github.cdimascio:dotenv-kotlin:6.2.2")
    implementation("com.auth0:java-jwt:3.18.1")
    implementation ("io.perfmark:perfmark-api:0.24.0")

}


tasks.withType<Jar> {
    manifest {
        attributes["Main-Class"] = "server.MainKt"
    }

    duplicatesStrategy = DuplicatesStrategy.EXCLUDE

    configurations["compileClasspath"].forEach { file: File ->
        from(zipTree(file.absoluteFile))
    }
}