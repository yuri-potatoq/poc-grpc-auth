import com.google.protobuf.gradle.generateProtoTasks
import com.google.protobuf.gradle.id
import com.google.protobuf.gradle.plugins
import com.google.protobuf.gradle.protobuf
import com.google.protobuf.gradle.protoc

plugins {
    id("com.google.protobuf")
    kotlin("jvm")

    idea
    application
    java
}


sourceSets.main {
    java.srcDirs("src/main/proto")
}

//
//tasks.whenTaskAdded {
//    if (this.name.startsWith("generate") && this.name.endsWith("Proto")) {
//        this.enabled = false
//    }
//}

dependencies {
    implementation ("io.grpc:grpc-kotlin-stub:1.1.0")

    implementation ("com.google.protobuf:protobuf-java:${rootProject.ext["protobufVersion"]}")
    implementation ("io.grpc:grpc-okhttp:${rootProject.ext["grpcVersion"]}")
    implementation ("io.grpc:grpc-protobuf:${rootProject.ext["grpcVersion"]}")

    implementation ("io.grpc:grpc-kotlin-stub:${rootProject.ext["grpcKotlinVersion"]}")
    implementation ("io.grpc:grpc-stub:${rootProject.ext["grpcKotlinVersion"]}")
}

protobuf {
    protoc {
        artifact = "com.google.protobuf:protoc:${rootProject.ext["protobufVersion"]}"
    }
    plugins {
        id("grpc") {
            artifact = "io.grpc:protoc-gen-grpc-java:${rootProject.ext["grpcVersion"]}"
        }
        id("grpckt") {
            artifact = "io.grpc:protoc-gen-grpc-kotlin:${rootProject.ext["grpcKotlinVersion"]}:jdk7@jar"
        }
    }

//    generatedFilesBaseDir = "${projectDir}/src/"

    generateProtoTasks {
        all().forEach {

            it.plugins {
                id("grpc")
                id("grpckt")
            }
        }
    }
}

